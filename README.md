# Hnefatafl
A little app for pass-and-play
[Tafl games](https://en.wikipedia.org/wiki/Tafl_games).

## Contents
1. [Installation](#installation)
2. [Usage](#usage)
3. [Documentation](#documentation)
4. [Contributing](#contributing)
5. [Credits](#credits)
6. [License](#license)

## Installation
[Documentation needed]

## Usage
[Documentation needed]

## Documentation
[Documentation needed]

## Contributing
[Documentation needed]

## Credits
**Assets**
- [Icons]

**Dependencies**
- [Piston]

## License
Dual-licensed to be compatible with the Rust project.

This software and associated documentation files are licensed under one of the
following:

- [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0)
- [MIT License](https://opensource.org/licenses/MIT)

at your option. This file may not be copied, modified, or distributed except
according to those terms.
