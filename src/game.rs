//!

// need to somehow modularize rulesets

use crate::board::{Board, Player};
use crate::flatgrid::Coord;

//////////////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////////////

#[derive(Clone, Copy, Debug)]
pub(crate) enum GameVariant {
    AleaEvangelii,
    Brandub,
    Copenhagen,
    Ealdfaeder,
    Fetlar,
    SeaBattle,
    SingleStep,
    Tablut,
    Tawlbwrdd(usize),
    York,
}

#[derive(Debug)]
pub(crate) enum GameState {
    Active(Player),
    Over(Option<Player>),
}

//////////////////////////////////////////////////////////////////////////////
// Game Context
//////////////////////////////////////////////////////////////////////////////

#[derive(Debug)]
pub(crate) struct GameContext {
    board: Board,
    ruleset: GameVariant,
    state: GameState,
    selection: Option<Coord>,
}

impl GameContext {
    //////////////////////////////////
    // Instantiation
    //////////////////////////////////

    pub(crate) fn new(variant: GameVariant) -> Self {
        Self {
            board: Board::new(variant),
            ruleset: variant,
            state: GameState::Active(Player::Black),
            selection: None,
        }
    }

    //////////////////////////////////
    // Pre-Move Phase
    //////////////////////////////////

    pub(crate) fn try_select(&mut self, pos: Coord) {
        // check if there exists at that point a piece and that that piece is
        // owned by the current player
        unimplemented!();
    }

    pub(crate) fn deselect(&mut self) {
        self.selection = None;
    }

    pub(crate) fn selection_moves(&mut self) -> Vec<Coord> {
        // use ruleset to determine which queries to run
        unimplemented!();
    }

    pub(crate) fn all_moves(&mut self) -> Vec<Coord> {
        // use ruleset to determine which queries to run
        unimplemented!();
    }

    pub(crate) fn attempt_move(&mut self, dst: &Coord) {
        if self.selection_moves().contains(dst) {
            unimplemented!();
            //  perform move
                // make a method for GameContext with handles logging and
                // moves simulataneously
            //  check for a win
            //  perform captures
            //  update gamestate

            // aside -- clicking on another one of your pieces should select
            // it
        } else {
            self.deselect();
        }
    }

    //////////////////////////////////
    // Post-Move Phase
    //////////////////////////////////

    pub(crate) fn check_win(&mut self) {
        // use ruleset to determine which queries to run
        unimplemented!();
        // this function prototype might be a bit off
        // might make it 'update gameState' or something
    }

    pub(crate) fn perform_captures(&mut self) {
        unimplemented!();
    }
}

//////////////////////////////////////////////////////////////////////////////
// Unit Tests
//////////////////////////////////////////////////////////////////////////////

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_it_works() {
        assert_eq!(1 + 1, 2);
    }
}
