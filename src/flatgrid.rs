//! Generic two-dimensional grid.

//////////////////////////////////////////////////////////////////////////////
// Type Aliases
//////////////////////////////////////////////////////////////////////////////

/// Zero-indexed coordinate of the form (column, row).
pub type Coord = (usize, usize);

//////////////////////////////////////////////////////////////////////////////
// Fixed-Size 2D Grids
//////////////////////////////////////////////////////////////////////////////

/// Two-dimensional, zero-indexed grid. Cannot change dimensions.
#[derive(Debug)]
pub struct StaticGrid<T> {
    col_count: usize,
    row_count: usize,
    data: Vec<T>,
}

impl<T> StaticGrid<T> {
    //////////////////////////////////
    // Utilities
    //////////////////////////////////

    /// Get the flat-vector index from the column and row indices.
    fn flat_index(&self, (col, row): Coord) -> usize {
        col + self.col_count * row
    }

    //////////////////////////////////
    // Instantiation
    //////////////////////////////////

    /// Create a new `StaticGrid` populated with a default value.
    pub fn new(col_count: usize, row_count: usize, default: T) -> Self
    where
        T: Copy,
    {
        let capactiy = row_count * col_count;

        Self {
            col_count: col_count,
            row_count: row_count,
            data: vec![default; capactiy],
        }
    }

    /// Create a new `StaticGrid` in a square shape, populated with a default
    /// value.
    pub fn square(side_len: usize, default: T) -> Self
    where
        T: Copy,
    {
        Self::new(side_len, side_len, default)
    }

    //////////////////////////////////
    // Get & Set
    //////////////////////////////////

    /// Get an immutable reference to a cell's value.
    pub fn get(&self, coord: Coord) -> Option<&T> {
        if self.contains(coord) {
            let index = self.flat_index(coord);

            Some(&self.data[index])
        } else {
            None
        }
    }

    /// Get a mutable reference to a cell's value.
    pub fn get_mut(&mut self, coord: Coord) -> Option<&mut T> {
        if self.contains(coord) {
            let index = self.flat_index(coord);

            Some(&mut self.data[index])
        } else {
            None
        }
    }

    /// Set a cell's value.
    pub fn set(&mut self, coord: Coord, new_val: T) {
        match self.get_mut(coord) {
            Some(val) => {
                *val = new_val;
            }
            None => (),
        }
    }

    //////////////////////////////////
    // Boolean Operations
    //////////////////////////////////

    /// Determine if a coordinate is within the grid
    pub fn contains(&self, (col, row): Coord) -> bool {
        col < self.col_count && row < self.row_count
    }
}

//////////////////////////////////////////////////////////////////////////////
// Unit Tests
//////////////////////////////////////////////////////////////////////////////

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_mut() {
        let mut grid = StaticGrid::new(1, 1, 'a');
        let value = grid.get_mut((0, 0)).unwrap();

        assert_eq!(&'a', value);
        *value = 'b';
        assert_eq!(&'b', value);
    }

    #[test]
    fn test_get_set() {
        let mut grid = StaticGrid::new(5, 5, 'a');

        assert_eq!(Some(&'a'), grid.get((2, 3)));
        assert_eq!(Some(&'a'), grid.get((3, 3)));
        assert_eq!(Some(&'a'), grid.get((3, 4)));

        grid.set((2, 3), 'b');
        grid.set((3, 3), 'c');
        grid.set((3, 4), 'd');

        assert_eq!(Some(&'b'), grid.get((2, 3)));
        assert_eq!(Some(&'c'), grid.get((3, 3)));
        assert_eq!(Some(&'d'), grid.get((3, 4)));
    }
}
