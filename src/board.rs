//! Hnefatafl game.

pub use crate::flatgrid::Coord;

use crate::flatgrid::StaticGrid;
use crate::game::{GameVariant, GameVariant::*};

//////////////////////////////////////////////////////////////////////////////
// Miscellaneous Game Elements
//////////////////////////////////////////////////////////////////////////////

/// Player ownership labels.
#[derive(Clone, Copy, Debug)]
pub(crate) enum Player {
    White,
    Black,
}

/// Game pieces.
#[derive(Clone, Copy, Debug)]
pub(crate) enum Piece {
    Crown,
    Sword(Player),
}

/// Space attributes.
#[derive(Clone, Copy, Debug)]
pub(crate) enum Terrain {
    Normal,
    Throne,
    Castle,
}

//////////////////////////////////////////////////////////////////////////////
// Board Cells
//////////////////////////////////////////////////////////////////////////////

/// Contents and attributes of a given position on the board.
#[derive(Clone, Copy, Debug)]
struct Cell {
    piece: Option<Piece>,
    terrain: Terrain,
}

impl Default for Cell {
    fn default() -> Self {
        Self {
            piece: None,
            terrain: Terrain::Normal,
        }
    }
}

//////////////////////////////////////////////////////////////////////////////
// Game Board (Model)
//////////////////////////////////////////////////////////////////////////////

#[derive(Debug)]
pub(crate) struct Board {
    cells: StaticGrid<Cell>,
    side_len: usize,
}

/// Generate a board from a square vector of strings (meaning each string is
/// the same length and there are the same number of strings as elements in
/// the vector). Support is offered for reading in what might be illegal board
/// positions.
///
/// Key:
///
///          Unit
/// Terrain           Standard     Castle     Throne
///          Black       b           N          B
///          White       w           E          W
///          King        k           *          K
impl From<Vec<&str>> for Board {
    fn from(blueprint: Vec<&str>) -> Self {
        // Establish board dimensions
        let slen = blueprint.len();
        let mut board = StaticGrid::square(slen, Cell::default());

        // Read-in tile values
        let mut row_index = 0;
        for line in &blueprint {
            let mut col_index = 0;

            for symbol in line.chars() {
                let mut cell = board.get_mut((col_index, row_index)).unwrap();

                match symbol {
                    // Empty special tiles
                    '+' => {
                        cell.terrain = Terrain::Castle;
                    }
                    't' => {
                        cell.terrain = Terrain::Throne;
                    }

                    // Units on standard tiles
                    'b' => {
                        cell.piece = Some(Piece::Sword(Player::Black));
                    }
                    'w' => {
                        cell.piece = Some(Piece::Sword(Player::White));
                    }
                    'k' => {
                        cell.piece = Some(Piece::Sword(Player::White));
                    }

                    // Units on the throne
                    'B' => {
                        cell.terrain = Terrain::Throne;
                        cell.piece = Some(Piece::Sword(Player::Black));
                    }
                    'W' => {
                        cell.terrain = Terrain::Throne;
                        cell.piece = Some(Piece::Sword(Player::White));
                    }
                    'K' => {
                        cell.terrain = Terrain::Throne;
                        cell.piece = Some(Piece::Crown);
                    }

                    // Units on a castle
                    'N' => {
                        cell.terrain = Terrain::Castle;
                        cell.piece = Some(Piece::Sword(Player::Black));
                    }
                    'E' => {
                        cell.terrain = Terrain::Castle;
                        cell.piece = Some(Piece::Sword(Player::White));
                    }
                    '*' => {
                        cell.terrain = Terrain::Castle;
                        cell.piece = Some(Piece::Crown);
                    }

                    // Unrecognized symbol
                    _ => (),
                }
                col_index += 1;
            }
            row_index += 1;
        }

        // Return
        Self {
            cells: board,
            side_len: slen,
        }
    }
}

impl Board {
    //////////////////////////////////
    // Common Board Layouts
    //////////////////////////////////

    // const ALEA_EVANGELII: [&'static str; 11] = [];

    const BRANDUB: [&'static str; 7] = [
        "+..b..+",
        "...b...",
        "...w...",
        "bbwKwbb",
        "...w...",
        "...b...",
        "+..b..+",
    ];

    const COPENHAGEN: [&'static str; 11] = [
        "+..bbbbb..+",
        ".....b.....",
        "...........",
        "b....w....b",
        "b...www...b",
        "bb.wwKww.bb",
        "b...www...b",
        "b....w....b",
        "...........",
        ".....b.....",
        "+..bbbbb..+",
    ];

    // const EALDFAEDER: [&'static str; 11] = [];
    // const IMPERIAL: [&'static str; 9] = [];
    // const SEA_BATTLE: [&'static str; 11] = [];
    // const SINGLE_STEP: [&'static str; 11] = [];
    // const TABLUT: [&'static str; 11] = [];

    const TABLUT: [&'static str; 9] = [
        "+..bbb..+",
        "....b....",
        "....w....",
        "b...w...b",
        "bbwwKwwbb",
        "b...w...b",
        "....w....",
        "....b....",
        "+..bbb..+",
    ];

    // const TAWLBWRDD_9: [&'static str; 9] = [];
    // const TAWLBWRDD_11: [&'static str; 11] = [];
    // const YORK: [&'static str; 11] = [];

    //////////////////////////////////
    // Instantiation
    //////////////////////////////////

    /// Setup a new game of a standard variant.
    pub(crate) fn new(variant: GameVariant) -> Self {
        match variant {
            AleaEvangelii => unimplemented!(),
            Brandub => unimplemented!(),
            Copenhagen => Self::from(Self::COPENHAGEN.to_vec()),
            Ealdfaeder => unimplemented!(),
            Fetlar => Self::from(Self::COPENHAGEN.to_vec()),
            SeaBattle => unimplemented!(),
            SingleStep => unimplemented!(),
            Tablut => Self::from(Self::TABLUT.to_vec()),
            Tawlbwrdd(9) => unimplemented!(),
            Tawlbwrdd(11) => unimplemented!(),
            York => unimplemented!(),
            _ => panic!("unsupported layout"),
        }
    }

    //////////////////////////////////
    // Examination
    //////////////////////////////////

    /// Return the piece at the given coordinate.
    fn piece(&self, pos: Coord) -> Option<Piece> {
        match self.cells.get(pos) {
            Some(&cell) => cell.piece,
            _ => None,
        }
    }

    /// Return the terrain at the given coordinate.
    fn terrain(&self, pos: Coord) -> Option<Terrain> {
        match self.cells.get(pos) {
            Some(&cell) => Some(cell.terrain),
            _ => None,
        }
    }

    //////////////////////////////////
    // Basic Actions
    //////////////////////////////////

    /// Set the piece for a given cell.
    fn set_piece(&mut self, pos: Coord, piece: Option<Piece>) {
        if let Some(cell) = self.cells.get_mut(pos) {
            cell.piece = piece;
        }
    }

    /// Dumbly move any piece at `src` to `dst` if `src` contains a piece and
    /// `dst` is on the board. Removes any pieces already at `dst`.
    fn mv(&mut self, src: Coord, dst: Coord) {
        if let Some(piece) = self.piece(src) {
            if self.cells.contains(dst) {
                self.set_piece(src, None);
                self.set_piece(src, Some(piece));
            }
        }
    }

    /// Remove any piece at the given coordinate.
    fn rm(&mut self, pos: Coord) {
        self.set_piece(pos, None);
    }
}

//////////////////////////////////////////////////////////////////////////////
// Unit Tests
//////////////////////////////////////////////////////////////////////////////

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_it_works() {
        assert_eq!(1 + 1, 2);
    }
}