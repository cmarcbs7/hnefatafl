//! Viking chess
//!
//! Builds an executable binary.

// add hover/click color changes
// let user determine game variant
// scale window to grid size

mod board;
mod game;
mod flatgrid;
mod log;

fn main() {
    unimplemented!();
}

/*
use piston::{
    window::WindowSettings,
    event_loop::*,
    input::*,
};
use graphics;
use glutin_window::GlutinWindow as Window;
use opengl_graphics::{ GlGraphics, OpenGL };

use std::path::PathBuf;

use board::Board;

//////////////////////////////////////////////////////////////////////////////
// Application
//////////////////////////////////////////////////////////////////////////////

struct App {
    ctx: GameContext,
    gl: GlGraphics,
    win: Window,
}

impl App {
    //////////////////////////////////
    // Instantiation
    //////////////////////////////////

    fn new() -> Self {
        // Graphics library
        let opengl = OpenGL::V3_2;

        // Primary game window
        let mut game_window = WindowSettings::new(
            "Hnefatafl",
            [750, 750],
        )
        .graphics_api(opengl)
        .exit_on_esc(true)
        .build()
        .unwrap();

        // Instantiation
        Self {
            ctx: GameContext::new(GameVariant::Copenhagen),
            gl: GlGraphics::new(opengl),
            win: game_window,
        }
    }

    //////////////////////////////////
    // Update
    //////////////////////////////////

    fn update(&mut self, args: &UpdateArgs) {
        /* code here */
    }

    //////////////////////////////////
    // Render
    //////////////////////////////////

    fn render(&mut self, args: &RenderArgs) {
        use graphics::*;

        let config = &self.rconfig;

        // Determine the offset to center items
        let total_width =
            config.cell_side_len * self.ctx.board.side_len() as f64
            + config.padding * (self.ctx.board.side_len() - 1) as f64;
        let half_width = total_width / 2.0;

        let base_center_x = args.window_size[0] / 2.0;
        let base_center_y = args.window_size[1] / 2.0;

        let offset_x = base_center_x - half_width;
        let offset_y = base_center_y - half_width;

        // Single-tile dimensions
        let side_dim = self.ctx.board.side_len;
        let side_len = 50.0;
        let spacing = 5.0;
        let rel_offset = side_len + spacing;

        // Board dimensions

        // Window dimensions

        let tile_template = rectangle::square(0.0, 0.0, config.cell_side_len);

        self.gl.draw(args.viewport(), |c, gl| {
            clear(config.bg_color, gl);

            for col in 0..side_dim {
                for row in 0..side_dim {
                    let col_offset =
                        (col as f64)
                        * (config.padding + config.cell_side_len);
                    let row_offset =
                        (row as f64)
                        * (config.padding + config.cell_side_len);

                    let transform =
                        c.transform
                        .trans(offset_x, offset_y)
                        .trans(col_offset, row_offset);

                    rectangle(
                        config.cell_color,
                        tile_template,
                        transform,
                        gl
                    );

                    Rectangle::new_border(config.border_color, 1.0)
                    .draw(tile_template, &c.draw_state, transform, gl);
                }
            }
        });
    }
}

fn main() {
    let mut app = App::new();
    let mut events = Events::new(EventSettings::new());

    while let Some(event) = events.next(&mut app.win) {
        if let Some(r) = event.render_args() {
            app.render(&r);
        }
        if let Some(u) = event.update_args() {
            app.update(&u);
        }
    }
}
*/
