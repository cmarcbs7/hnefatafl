//!

use crate::board::{Piece, Coord};
use crate::game::{GameState, GameVariant};

#[derive(Debug)]
struct Move {
    piece: Piece,
    src: Coord,
    dst: Coord,
    capture: Option<Piece>,
    outcome: GameState,
}

#[derive(Debug)]
struct Log {
    record: Vec<Move>,
    ruleset: GameVariant,
}

impl Log {
    //////////////////////////////////
    // Instantiation
    //////////////////////////////////

    /// Create a new and empty `Log`.
    fn new(variant: GameVariant) -> Self {
        Self {
            record: Vec::new(),
            ruleset: variant,
        }
    }
}
